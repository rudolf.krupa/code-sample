#!/usr/bin/env php
<?php

declare(strict_types=1);

use CodeSample\Di\DependencyInjection;
use CodeSample\App;
use CodeSample\Config;
use CodeSample\Exception\SwooleClassMissing;

require __DIR__ . '/../vendor/autoload.php';

if (!class_exists('Swoole\WebSocket\Server')) {
    throw new SwooleClassMissing();
}

$config = new Config(__DIR__);
$dependencyInjection = new DependencyInjection($config);
(new App($dependencyInjection))->run();