<?php

declare(strict_types=1);

namespace CodeSample\Models;

use Swoole\Table;

class Messages extends Table {

    /*
    public function __costruct($size = 1024)
    {
        parent::__costruct($size);
    }
    */

    public function make() {
        $this->column('id', Table::TYPE_INT, 11);
        $this->column('client', Table::TYPE_INT, 4);
        $this->column('username', Table::TYPE_STRING, 64);
        $this->column('message', Table::TYPE_STRING, 255);
        $this->create();
    }

    /**
     * @param int $id
     */
    public function add(int $id): void
    {
        $this->set($id, ['id' => $id]);
    }

    /**
     * @param int $id
     */
    public function remove(int $id): void
    {
        $this->del($id);
    }

    public function all() {

//        foreach($this as $row) {
//            var_dump($row);
//        }
        echo count($this);
    }
}