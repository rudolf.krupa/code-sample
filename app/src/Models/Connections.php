<?php

namespace CodeSample\Models;

use Swoole\Table;

class Connections extends Table
{
    public function make() {
        $this->column('client', Table::TYPE_INT, 4);
        $this->create();
    }
}