<?php

declare(strict_types=1);

namespace CodeSample;

use CodeSample\Server\HttpWsServerManager;
use CodeSample\Di\DependencyInjection;

class App
{
    /** @const string */
    const HTTP_WS_SERVER_MANAGER_SERVICE = 'httpWsServerManager';

    /** @var DependencyInjection */
    private $dependencyInjection;

    /** @var HttpWsServerManager */
    private $httpWsServerManager;

    /**
     * App constructor.
     * @param DependencyInjection $dependencyInjection
     */
    public function __construct(DependencyInjection $dependencyInjection)
    {
        $this->dependencyInjection = $dependencyInjection;
        $this->httpWsServerManager = $dependencyInjection->buildInstanceByName(self::HTTP_WS_SERVER_MANAGER_SERVICE);
    }

    public function run(): void
    {
        $this->httpWsServerManager->run($this->dependencyInjection);
    }
}