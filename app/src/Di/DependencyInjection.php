<?php

declare(strict_types=1);

namespace CodeSample\Di;

use ArrayObject;
use ReflectionClass;
use ReflectionException;
use CodeSample\Config;
use CodeSample\Di\Exception\InvalidServiceName;
use CodeSample\Di\Exception\DuplicateServices;

final class DependencyInjection extends ArrayObject
{
    /** @const string */
    const CONFIG_SERVICE_NAME = 'config';

    /** @const string */
    const CONSTRUCTOR_METHOD = '__construct';

    /** @const string */
    const NAMESPACE_DELIMITER = '\\';

    /** @const array */
    const DATA_TYPES = [
        'int',
        'integer',
        'string',
        'float',
        'array',
        'bool',
        'boolean'
    ];

    /** @var array */
    private $services = [];

    /**
     * DependencyInjection constructor.
     * @param Config $config
     * @throws DuplicateServices
     * @throws Exception\MissingListOfServices
     */
    public function __construct(Config $config)
    {
        parent::__construct();

        $diConfig = new DiConfig($config);
        $this->services = $diConfig->getServices();

        $duplicateServices = $this->findDuplicate($this->services);
        if (!empty($duplicateServices)) {
            throw new DuplicateServices(implode(' | ', $duplicateServices));
        }

        $this->registerService(self::CONFIG_SERVICE_NAME, $config);
    }

    /**
     * @param array $items
     * @return array
     */
    private function findDuplicate(array $items): array
    {
        return array_unique(array_diff_assoc($items, array_unique($items)));
    }

    /**
     * @param string $className
     * @return object
     * @throws ReflectionException
     */
    public function buildInstanceByClassNameFactory(string $className): object
    {
        $className = $this->normalizeClassNamespace($className);

        $reflectionClass = new ReflectionClass($className);

        $instanceParams = [];
        if ($reflectionClass->hasMethod(self::CONSTRUCTOR_METHOD)) {
            $parameters = $reflectionClass->getMethod(self::CONSTRUCTOR_METHOD)->getParameters();

            if(!empty($parameters)) {
                foreach ($parameters as $param) {
                    $paramType = $this->normalizeClassNamespace((string) $param->getType());
                    if($this->hasInstance($paramType)) {
                        $instanceParams[] = $this->offsetGet($this->getServiceNameByClassName($paramType));
                    } else {
                        $instanceParams[] = $this->buildInstanceByClassNameFactory($paramType);
                    }
                }
            }
        }

        $instance = $reflectionClass->newInstanceArgs($instanceParams);

        $serviceName = $this->getServiceNameByClassName($className);
        if (!empty($serviceName)) {
            $this[$serviceName] = $instance;
        }

        return $instance;
    }

    /**
     * @param string $name
     * @return object
     * @throws InvalidServiceName
     * @throws ReflectionException
     */
    public function buildInstanceByName(string $name): object
    {
        $result = null;
        if ($this->offsetExists($name)) {
            $result = $this->offsetGet($name);
        } else {
            $className = $this->getServiceClassByName($name);
            $result = $this->buildInstanceByClassNameFactory($className);
        }

        return $result;
    }

    /**
     * @param string $paramType
     * @return string
     */
    private function normalizeClassNamespace(string $paramType): string
    {
        $subParamType = strtolower($paramType);
        if (!in_array($subParamType, self::DATA_TYPES)) {
            $result = self::NAMESPACE_DELIMITER . trim($paramType, self::NAMESPACE_DELIMITER);
        } else {
            $result = $paramType;
        }

        return $result;
    }

    /**
     * @param string $className
     * @return string
     */
    private function getServiceNameByClassName(string $className): string
    {
        return array_search($className, $this->services,true);
    }

    /**
     * @param string $name
     * @return string
     * @throws InvalidServiceName
     */
    private function getServiceClassByName(string $name): string
    {
        if (!isset($this->services[$name])) {
            throw new InvalidServiceName($name);
        }

        return $this->services[$name];
    }

    /**
     * @param string $className
     * @return bool
     */
    private function hasInstance(string $className): bool
    {
        $serviceName = $this->getServiceNameByClassName($className);
        return $this->offsetExists($serviceName) && $this[$serviceName] instanceof $className;
    }

    /**
     * @param string $name
     * @param object $object
     */
    public function registerService(string $name, object $object): void
    {
        $this->offsetSet($name, $object);
    }
}