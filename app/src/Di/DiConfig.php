<?php

declare(strict_types=1);

namespace CodeSample\Di;

use CodeSample\Config;
use CodeSample\Di\Exception\MissingListOfServices;

class DiConfig
{
    /** @const string */
    const NAME_LIST_OF_SERVICES = 'services';

    /** @var array */
    private $services = [];

    /**
     * DiConfig constructor.
     * @param Config $config
     * @throws MissingListOfServices
     */
    public function __construct(Config $config)
    {
        if (!isset($config[self::NAME_LIST_OF_SERVICES])) {
            throw new MissingListOfServices("By key " . self::NAME_LIST_OF_SERVICES);
        }
        $this->services = $config[self::NAME_LIST_OF_SERVICES];
    }

    /**
     * @return array
     */
    public function getServices(): array
    {
        return $this->services;
    }
}