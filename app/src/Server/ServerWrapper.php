<?php

declare(strict_types=1);

namespace CodeSample\Server;

use CodeSample\Di\DependencyInjection;
use Swoole\WebSocket\Server as SwooleServer;

class ServerWrapper extends SwooleServer
{
    /** @var DependencyInjection */
    private $dependencyInjection;

    /**
     * @param EventHandler $eventHandler
     */
    public function registerEvents(EventHandler $eventHandler): void
    {
        foreach($eventHandler->getEventWithListenerMethods() as $eventKey => $eventMethod){
        	$eventHandler->server = $this;

            $dependencyInjection = $this->dependencyInjection;
//            if($eventKey == 'request') {
        	    $this->on($eventKey, function() use ($eventHandler, $eventMethod, $dependencyInjection) {
                    $eventHandler->dependencyInjection = $dependencyInjection;
                    call_user_func_array([$eventHandler, $eventMethod], func_get_args());
                });
//            } else {
//                $this->on($eventKey, [$eventHandler, $eventMethod]);
//            }
        }
    }

    /**
     * @param DependencyInjection $dependencyInjection
     */
    public function setDependencyInjection(DependencyInjection $dependencyInjection): void
    {
        $this->dependencyInjection = $dependencyInjection;
    }

    /**
     * @return DependencyInjection
     */
    public function getDependencyInjection(): DependencyInjection
    {
        return $this->dependencyInjection;
    }
}