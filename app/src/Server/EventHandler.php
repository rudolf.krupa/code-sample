<?php

declare(strict_types=1);

namespace CodeSample\Server;

use CodeSample\Config;
use CodeSample\Connections;
use CodeSample\Di\DependencyInjection;
use CodeSample\Server\Entity\DummyResponse;
use Swoole\Http\Request as SwooleRequest;
use Swoole\Http\Response as SwooleResponse;
use Swoole\Server;
use Swoole\WebSocket\Frame as SwooleFrame;
use CodeSample\Server\Entity\DummyRequest;
use CodeSample\Server\Entity\DummyFileResponse;
USE Swoole\Http\Status;

final class EventHandler
{
    /** @cost string */
    const START = 'start';

    /** @cost string */
    const REQUEST = 'request';

    /** @cost string */
    const OPEN = 'open';

    /** @cost string */
    const CLOSE ='close';

    /** @cost string */
    const MESSAGE = 'message';

    /** @var string[] */
    private $events = [
        self::START,
        self::REQUEST,
        self::OPEN,
        self::MESSAGE,
        self::CLOSE
    ];

    /** @var ServerWrapper */
    public $server;

    /** @var string[] */
    private $eventWithListenerMethods = [];

    // todo: remove later -> only for path to www dir
    /** @var Config */
    private $config;

    /** @var DependencyInjection */
    public $dependencyInjection;

    public $connectionsStorage;

    /**
     * EventHandler constructor.
     * @param Config $config
     */
    public function __construct(Config $config)
    {
        $this->config = $config;

        $this->connectionsStorage = new Connections(1024 * 1024);

        // todo: validate if there is method for each event
        foreach($this->events as $event) {
            $this->eventWithListenerMethods[$event] = $this->getEventMethod($event);
        }
    }

    /**
     * @param ServerWrapper $server
     */
    public function onStart(ServerWrapper $server): void
    {
        echo sprintf("server start on: %s:%s", $server->host, $server->port) . PHP_EOL;
    }

    /**
     * @param ServerWrapper $server
     * @param SwooleRequest $request
     */
    public function onOpen(ServerWrapper $server, SwooleRequest $request): void
    {
        echo sprintf("connection open: %d", $request->fd) . PHP_EOL;

//        $this->connectionsStorage->add($request->fd);
    }

    /**
     * @param SwooleRequest $swooleRequest
     * @param SwooleResponse $swooleResponse
     */
    public function onRequest(SwooleRequest $swooleRequest, SwooleResponse $swooleResponse): void
    {
        $request = new DummyRequest($swooleRequest);

        // todo: make router for dynamic request & configure nginx for static files
        if (in_array($request->getRequestUri(), ['/', 'index.html'])) {
            $fileResponse = new DummyFileResponse($swooleResponse, $this->config);
            $fileResponse->sendFile(DummyFileResponse::INDEX);
        } elseif(in_array($request->getRequestUri(), ['/connections'])) {
            $response = new DummyResponse($swooleResponse);
            $response->send([
                'connections' => 'sampleConnections'
            ]);

//            $this->connectionsStorage->all();

        } elseif(in_array($request->getRequestUri(), ['/stats'])) {
            $response = new DummyResponse($swooleResponse);
            $response->send([
                'stats' => $this->server->stats()
            ]);
        } else {
            $fileResponse = new DummyFileResponse($swooleResponse, $this->config);
            $fileResponse->sendFile(DummyFileResponse::NOT_FOUND, Status::NOT_FOUND);
        }
    }

    /**
     * @param ServerWrapper $server
     */
    public function onClose(ServerWrapper $server): void
    {
        echo "close" . PHP_EOL;
    }

    /**
     * @param string $methodName
     * @return string
     */
    private function getEventMethod(string $methodName): string
    {
        return 'on' . ucfirst($methodName);
    }

    /**
     * @return array|string[]
     */
    public function getEventWithListenerMethods(): array
    {
        return $this->eventWithListenerMethods;
    }
}