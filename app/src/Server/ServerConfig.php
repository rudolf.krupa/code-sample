<?php

declare(strict_types=1);

namespace CodeSample\Server;

use CodeSample\Server\Exception\MissingConfiguration;
use CodeSample\Config;

class ServerConfig
{
    /** @cost string */
    const CONFIG_KEY = 'server';

    /** @cost string */
    const IP = 'ip';

    /** @cost string */
    const PORT = 'port';

    /** @var string */
    public $ip;

    /** @var string */
    public $port;

    /**
     * ServerConfig constructor.
     * @param Config $config
     * @throws MissingConfiguration
     */
    public function __construct(Config $config)
    {
        if (!isset($config[self::CONFIG_KEY])
            || !isset($config[self::CONFIG_KEY][self::IP])
            || !isset($config[self::CONFIG_KEY][self::PORT])) {
            throw new MissingConfiguration();
        }

        $this->ip = $config[self::CONFIG_KEY][self::IP];
        $this->port = $config[self::CONFIG_KEY][self::PORT];
    }
}