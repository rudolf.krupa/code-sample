<?php

declare(strict_types=1);

namespace CodeSample\Server\Entity;

use CodeSample\Config;
use Swoole\Http\Response as SwooleResponse;
USE Swoole\Http\Status;

final class DummyFileResponse extends Response
{
    /** @const string */
    const NOT_FOUND = '404';

    /** @const string */
    const INDEX = 'index';

    /** @var Config */
    private $config;

    /**
     * DummyFileResponse constructor.
     * @param SwooleResponse $swooleResponse
     * @param Config $config
     */
    public function __construct(SwooleResponse $swooleResponse, Config $config)
    {
        $this->swooleResponse = $swooleResponse;
        $this->config = $config;
    }

    /**
     * @param string $file
     * @param int $code
     */
    public function sendFile(string $file = self::NOT_FOUND, int $code = Status::NOT_FOUND): void
    {
        $this->swooleResponse->status($code);
        $this->swooleResponse->header("Content-Type", "text/html");
        $this->swooleResponse->sendFile($this->config['wwwDirectoryPath'] . $this->config::DS . 'www'. $this->config::DS  . $file . '.html');
    }
}