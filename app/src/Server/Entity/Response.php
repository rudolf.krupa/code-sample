<?php

declare(strict_types=1);

namespace CodeSample\Server\Entity;

use Swoole\Http\Response as SwooleResponse;

abstract class Response
{
    /** @var SwooleResponse */
    protected $swooleResponse;

    /**
     * DummyFileResponse constructor.
     * @param SwooleResponse $swooleResponse
     */
    public function __construct(SwooleResponse $swooleResponse)
    {
        $this->swooleResponse = $swooleResponse;
    }
}