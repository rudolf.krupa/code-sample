<?php

declare(strict_types=1);

namespace CodeSample\Server\Entity;

use Swoole\Http\Request as SwooleRequest;

final class DummyRequest
{
    /** @const STRING */
    const REQUEST_URI = 'request_uri';

    /** @var SwooleRequest */
    private $swooleRequest;

    /**
     * Request constructor.
     * @param SwooleRequest $swooleRequest
     */
    public function __construct(SwooleRequest $swooleRequest)
    {
        $this->swooleRequest = $swooleRequest;
    }

    /**
     * @return string|null
     */
    public function getRequestUri(): ?string
    {
        return isset($this->swooleRequest->server[self::REQUEST_URI]) ? $this->swooleRequest->server[self::REQUEST_URI] : null;
    }
}