<?php

declare(strict_types=1);

namespace CodeSample\Server\Entity;

final class DummyResponse extends Response
{
    public function send(array $data): void
    {
        $this->swooleResponse->header("Content-Type", "application/json");
        $this->swooleResponse->end(json_encode($data));
    }
}