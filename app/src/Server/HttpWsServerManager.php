<?php

declare(strict_types=1);

namespace CodeSample\Server;

use CodeSample\Di\DependencyInjection;

final class HttpWsServerManager
{
    /** @var ServerWrapper */
    private $serverWrapper;

    /** @var EventHandler */
    private $eventHandler;

    /**
     * HttpWsServerManager constructor.
     * @param ServerConfig $serverConfig
     * @param EventHandler $eventHandler
     */
    public function __construct(ServerConfig $serverConfig, EventHandler $eventHandler)
    {
        $this->serverWrapper = new ServerWrapper($serverConfig->ip, $serverConfig->port);
        $this->eventHandler = $eventHandler;
    }

    /**
     * @param DependencyInjection $dependencyInjection
     */
    public function run(DependencyInjection $dependencyInjection): void
    {
        $this->serverWrapper->setDependencyInjection($dependencyInjection);
        $this->serverWrapper->registerEvents($this->eventHandler);
        $this->serverWrapper->start();
    }
}