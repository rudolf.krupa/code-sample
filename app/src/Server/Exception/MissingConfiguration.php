<?php

declare(strict_types=1);

namespace CodeSample\Server\Exception;

use Exception;

final class MissingConfiguration extends Exception
{

}