<?php

declare(strict_types=1);

namespace CodeSample;

use ArrayObject;
use CodeSample\Exception\WrongConfigFilePath;

final class Config extends ArrayObject
{
    /** @cost string */
    const DS = DIRECTORY_SEPARATOR;

    /** @cost string */
    const CONF_DIRECTORY = 'config';

    /** @cost string */
    const CONF_FILE = 'main.yaml';

    /**
     * Config constructor.
     * @param string $path
     * @throws WrongConfigFilePath
     */
    public function __construct(string $path)
    {
        $wwwDirectoryPath = $path . self::DS . '..' . self::DS;

        $configFilePath =  $wwwDirectoryPath . self::CONF_DIRECTORY . self::DS . self::CONF_FILE;
        if (!file_exists($configFilePath)) {
            throw new WrongConfigFilePath($configFilePath);
        }

        $config = yaml_parse_file($configFilePath);
        $config['wwwDirectoryPath'] = $wwwDirectoryPath;
        parent::__construct($config);
    }

    /**
     * @param string $name
     * @return mixed
     */
    public function __get(string $name)
    {
        return (isset($this[$name]) ? $this[$name] : null);
    }

    /**
     * @param string $name
     * @param mixed $value
     */
    public function __set(string $name, $value): void
    {
        $this[$name] = $value;
    }
}