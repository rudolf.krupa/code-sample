<?php

declare(strict_types=1);

namespace CodeSample\Exception;

use  LogicException;

final class SwooleClassMissing extends LogicException
{

}