# Code sample

## Requreiments:
- php: 7.2+
- swoole: 4.5.2+    
  [link to swoole extension](https://www.swoole.co.uk/)

## run
- run composer install
- start docker: docker-compose up
- open in web browser [http://localhost](http://localhost)

## code check
- app/vendor/bin/phpstan analyse -l 5 app/src